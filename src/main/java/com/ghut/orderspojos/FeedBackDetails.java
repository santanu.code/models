package com.ghut.orderspojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FeedBackDetails {
    @JsonProperty("noOfStars")
    private int noOfStars;
    @JsonProperty("comment")
    private String comment;
}
