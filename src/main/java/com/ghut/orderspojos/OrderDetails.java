package com.ghut.orderspojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ghut.cartpojos.AddressDetails;
import com.ghut.cartpojos.BillingDetails;
import com.ghut.listingpojos.MainItem;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class OrderDetails {
    @JsonProperty("orderCreatedTimeStamp")
    private long orderCreatedTimeStamp;
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("orderStatus")
    private OrderStatus orderStatus;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("userEmailId")
    private String userEmailId;
    @JsonProperty("appCart")
    private Map<String, MainItem> appCart;
    @JsonProperty("address")
    private AddressDetails address;
    @JsonProperty("promocodeApplied")
    private String promocodeApplied;
    @JsonProperty("billingDetails")
    private BillingDetails billingDetails;
    @JsonProperty("feedbackDetails")
    private FeedBackDetails feedbackDetails;
    @JsonProperty("deliverySlot")
    private String deliverySlot;
}
