package com.ghut.orderspojos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LocationDetails {
    private double ourLocationLat;
    private double ourLocationLong;
    private double customerLocationLat;
    private double customerLocationLong;
}
