package com.ghut.orderspojos;


import com.ghut.utils.ConstantValues;

public enum OrderStatus {
    TO_BE_CONFIRMED("TO BE CONFIRMED"),
    CONFIRMED("CONFIRMED"),
    BEING_COOKED("BEING COOKED"),
    ON_THE_WAY("ON THE WAY"),
    DELIVERED("DELIVERED"),

    CANCELLED("CANCELLED");

    private String statusOfOrder;

    OrderStatus(String statusOfOrder) {
        this.statusOfOrder = statusOfOrder;
    }

    public final String getStatusOfOrder() {
        return statusOfOrder;
    }

    public static OrderStatus getEnumValue(String string) {
        switch (string) {
            case "TO BE CONFIRMED":return OrderStatus.TO_BE_CONFIRMED;
            case "CONFIRMED":return OrderStatus.CONFIRMED;
            case "BEING COOKED":return OrderStatus.BEING_COOKED;
            case "ON THE WAY":return OrderStatus.ON_THE_WAY;
            case "DELIVERED":return OrderStatus.DELIVERED;
            case "CANCELLED":return OrderStatus.CANCELLED;
            default: return null;
        }
    }

    public static String getNodeNameForOrderStatus(OrderStatus orderStatus) {
        switch (orderStatus) {
            case TO_BE_CONFIRMED: return ConstantValues.TO_BE_CONFIRMED_NODE_NAME;
            case CONFIRMED: return ConstantValues.CONFIRMED_NODE_NAME;
            case BEING_COOKED: return ConstantValues.BEING_COOKED_NODE_NAME;
            case ON_THE_WAY: return ConstantValues.ON_THE_WAY_NODE_NAME;
            case DELIVERED:return ConstantValues.DELIEVERED_NODE_NAME;
            case CANCELLED:return ConstantValues.CANCELLED_NODE_NAME;
            default:return null;
        }
    }
}
