package com.ghut.orderspojos;

import com.ghut.cartpojos.UserPromoCodeDetails;
import lombok.Data;

@Data
public class AllOrderDetails {
    private OrderDetails orderDetails;
    private UserPromoCodeDetails userPromoCodeDetails;
}
