package com.ghut.smspojos;

import lombok.Data;

@Data
public class SMSGatewayResponsePojo {
    private String message;
    private String type;
}
