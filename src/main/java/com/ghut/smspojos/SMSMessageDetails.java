package com.ghut.smspojos;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class SMSMessageDetails {
    private String message;
    private Set<String> to;
}
