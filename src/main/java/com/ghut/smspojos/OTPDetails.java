package com.ghut.smspojos;

import lombok.Data;

@Data
public class OTPDetails {
    private String mobileNumber;
    private String enteredOTPByCustomer;
}
