package com.ghut.smspojos;

import lombok.Data;

import java.util.List;

@Data
public class BulkSMSPayloadPojo {
    private String message;
    private String testModeOrProdMode;
    private List<String> testNumbers;

    public String getFormattedMessage() {
        return message.replaceAll(" ", "+");
    }
}
