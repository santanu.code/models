package com.ghut.smspojos;

import lombok.Data;

import java.util.List;

@Data
public class SMSDetailsPojo {
    private String message;
    private List<String> mobileNumbers;
}
