package com.ghut.smspojos;

import lombok.Data;

@Data
public class OTPResponseModel {
    private String message;
    private String type;

    public boolean checkTypeIsSuccess() {
        return type.equalsIgnoreCase("success");
    }
}
