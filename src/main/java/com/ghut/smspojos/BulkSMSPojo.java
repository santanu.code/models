package com.ghut.smspojos;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class BulkSMSPojo {
    private String sender;
    private String route;
    private String country;
    private List<SMSMessageDetails> sms;
}
