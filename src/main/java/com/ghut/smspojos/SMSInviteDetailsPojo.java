package com.ghut.smspojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SMSInviteDetailsPojo {
    private String sentTo;
    private String sentTimeStamp;
}
