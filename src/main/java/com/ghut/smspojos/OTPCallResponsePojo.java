package com.ghut.smspojos;

import lombok.Data;

@Data
public class OTPCallResponsePojo {
    private String message;
    private String type;
}
