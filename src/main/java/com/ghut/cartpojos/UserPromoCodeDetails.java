package com.ghut.cartpojos;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserPromoCodeDetails {
    public static final String NO_OF_TIMES_APPLIED_KEY = "noOfTimesApplied";
    private int noOfTimesApplied;
    private List<String> promocodeAppliedTS;
}
