package com.ghut.cartpojos;

import lombok.Data;

import java.util.List;

@Data
public class PromoCodeDetails {
    private boolean isValid;
    private boolean isEligible;
    private float discount;
    private String description;
    private List<String> scope;
    private PromoType promoType;

    public PromoCodeDetails(boolean isValid, boolean isEligible) {
        this.isValid = isValid;
        this.isEligible = isEligible;
    }
}
