package com.ghut.cartpojos;

import lombok.Data;

@Data
public class BillingDetails {
    private float subTotalAmount;
    private float packagingChargeAmount;
    private float deliveryChargeAmount;
    private float promocodeDiscount;
    private float finalBillAmount;
}
