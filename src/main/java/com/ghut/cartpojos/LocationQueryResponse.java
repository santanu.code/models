package com.ghut.cartpojos;

import lombok.Data;

@Data
public class LocationQueryResponse {
    private double distance;
    private int ETA;
}
