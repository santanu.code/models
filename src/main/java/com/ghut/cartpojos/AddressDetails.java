package com.ghut.cartpojos;

import lombok.Data;

@Data
public class AddressDetails {
    private double latitude;
    private double longitude;
    private String houseNumberFlatNoApartmentName;
    private String addressLine;
    private String nearbyLandMark;
    private String type;
}
