package com.ghut.navigationpojos;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.HashMap;

@Data
public class MenuCardDetailDialog {
    @SerializedName("items")
    private HashMap<String, String> items;
    @SerializedName("menuCardTitle")
    private String menuCardTitle;
}
