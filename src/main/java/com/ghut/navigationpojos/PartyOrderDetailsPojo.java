package com.ghut.navigationpojos;

public class PartyOrderDetailsPojo {
    private String userName;
    private String mobileNumber;
    private String noOfPlates;
    private String addedTimeStamp;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getNoOfPlates() {
        return noOfPlates;
    }

    public void setNoOfPlates(String noOfPlates) {
        this.noOfPlates = noOfPlates;
    }

    public String getAddedTimeStamp() {
        return addedTimeStamp;
    }

    public void setAddedTimeStamp(String addedTimeStamp) {
        this.addedTimeStamp = addedTimeStamp;
    }
}
