package com.ghut.navigationpojos;

import lombok.Data;

@Data
public class HelpDetailsPojo {
    private String userName;
    private String mobileNumber;
}
