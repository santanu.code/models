package com.ghut.navigationpojos;

import lombok.Data;

import java.util.Date;

@Data
public class UserData {
    private String userMobileNumber;
    private String extraInfo;
    private Date date;
}
