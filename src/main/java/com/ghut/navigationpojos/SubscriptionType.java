package com.ghut.navigationpojos;

import com.google.gson.annotations.SerializedName;

public enum SubscriptionType {
    @SerializedName("breakfast")
    BREAKFAST,
    @SerializedName("lunch")
    LUNCH,
    @SerializedName("dinner")
    DINNER;
}
