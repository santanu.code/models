package com.ghut.navigationpojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ghut.listingpojos.ItemListResponse;
import lombok.Data;

@Data
public class DishTypeDetails {
    @JsonProperty("veg")
    private ItemListResponse veg;
    @JsonProperty("non_veg")
    private ItemListResponse non_veg;
}

