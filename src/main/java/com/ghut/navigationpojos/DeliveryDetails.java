package com.ghut.navigationpojos;

import lombok.Data;

@Data
public class DeliveryDetails {
    private String deliveryTime;
    private int deliveryCharge;
    private boolean deliveryChargeIsEnabled;
}
