package com.ghut.navigationpojos;

import lombok.Data;

@Data
public class BookingDetails {
    private String userName;
    private String mobileNumber;
    private int noOfSlots;
    private String timeStamp;
}
