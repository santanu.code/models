package com.ghut.navigationpojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OfferDetails {
    @JsonProperty("isActive")
    private boolean isActive;
    @JsonProperty("tvHashTag")
    private String tvHashTag;
    @JsonProperty("tvMainMessage")
    private String tvMainMessage;
    @JsonProperty("tvMainTitle")
    private String tvMainTitle;
    @JsonProperty("tvOfferPriceDetail")
    private String tvOfferPriceDetail;
    @JsonProperty("menuCardDetailDialog")
    private MenuCardDetailDialog menuCardDetailDialog;

    @JsonProperty("isActive")
    public boolean isActive() {
        return isActive;
    }

    @JsonProperty("isActive")
    public void setActive(boolean active) {
        isActive = active;
    }
}
