package com.ghut.navigationpojos;

import lombok.Data;

import java.util.HashMap;

@Data
public class SubscriptionListResponse {
    private HashMap<SubscriptionType, DishTypeDetails> meals;
    private HashMap<SubscriptionType, DeliveryDetails> deliveryConfigs;
}
