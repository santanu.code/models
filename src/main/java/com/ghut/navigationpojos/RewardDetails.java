package com.ghut.navigationpojos;

import lombok.Data;

@Data
public class RewardDetails {
    private String apiKey;
    private String mobileNumber;
    private int rewardPoints;
}
