package com.ghut.usermgmtpojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetails {
    private String mobileNumber;
    private String userName;
    private String emailId;
    private String profileImageURL;
    private int rewardPoints;
    private String createdTimeStamp;
}
