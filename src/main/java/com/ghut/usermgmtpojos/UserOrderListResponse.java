package com.ghut.usermgmtpojos;

import com.ghut.orderspojos.OrderDetails;
import lombok.Data;

import java.util.HashMap;

@Data
public class UserOrderListResponse {

    private HashMap<String, OrderDetails> orders;

}
