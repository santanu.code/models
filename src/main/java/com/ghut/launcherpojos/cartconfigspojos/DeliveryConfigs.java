package com.ghut.launcherpojos.cartconfigspojos;

import lombok.Data;

@Data
public class DeliveryConfigs {
    private int deliveryCharge;
    private boolean deliveryChargeIsEnabled;
    private boolean packagingChargeIsEnabled;
    private int packagingCharge;
}
