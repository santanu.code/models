package com.ghut.launcherpojos.cartconfigspojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PromoCode {
    @JsonProperty("isActive")
    private boolean isActive;
    @JsonProperty("scope")
    private List<String> scope;
    @JsonProperty("type")
    private PromoDiscountType type;
    @JsonProperty("value")
    private int value;
    @JsonProperty("description")
    private String description;

    @JsonProperty("isActive")
    public boolean isActive() {
        return isActive;
    }

    @JsonProperty("isActive")
    public void setActive(boolean active) {
        isActive = active;
    }
}
