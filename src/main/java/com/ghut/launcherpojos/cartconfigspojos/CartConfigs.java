package com.ghut.launcherpojos.cartconfigspojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class CartConfigs {
    @JsonProperty("deliveryConfigs")
    private DeliveryConfigs deliveryConfigs;
    @JsonProperty("minOrderValue")
    private int minOrderValue;
    @JsonProperty("promoConfigs")
    private Map<String, PromoCode> promoConfigs;
    @JsonProperty("cartFooterAd")
    private String cartFooterAd;
}
