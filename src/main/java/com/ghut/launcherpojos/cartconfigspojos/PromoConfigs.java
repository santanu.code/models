package com.ghut.launcherpojos.cartconfigspojos;

import lombok.Data;

@Data
public class PromoConfigs {
    private PromoCode promoConfigs;
}
