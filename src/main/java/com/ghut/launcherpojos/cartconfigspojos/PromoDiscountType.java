package com.ghut.launcherpojos.cartconfigspojos;

public enum PromoDiscountType {
    PERCENTAGE, VALUE
}
