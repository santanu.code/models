package com.ghut.launcherpojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DeliverySlotConfigs {
    @JsonProperty("isActive")
    private boolean isActive;
    private int firstSlotGap;
    private int secondSlotGap;
    private int thirdSlotGap;
    private int defaultSlotGap;
    private int slotOffset;

    @JsonProperty("isActive")
    public boolean isActive() {
        return isActive;
    }

    @JsonProperty("isActive")
    public void setActive(boolean active) {
        isActive = active;
    }
}
