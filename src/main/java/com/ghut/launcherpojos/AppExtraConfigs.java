package com.ghut.launcherpojos;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class AppExtraConfigs {
    @SerializedName("menu-bar-msg")
    private String menuBarMessage;
}
