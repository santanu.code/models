package com.ghut.launcherpojos;

import com.ghut.launcherpojos.cartconfigspojos.CartConfigs;
import com.ghut.listingpojos.DialogDetails;
import com.ghut.listingpojos.MenuConfigs;
import com.ghut.listingpojos.SnackBarDetails;
import com.ghut.navigationpojos.OfferDetails;
import lombok.Data;

import java.util.Date;

@Data
public class AppConfigDetails {
    private float appVersionNumber;
    private boolean canBeSkipped;
    private int maxVersionDifferenceAllowedWhenSkipped;
    private boolean mandatoryUpdate;
    private boolean alreadyLatest;
    private String menuBarMessage;
    private DialogDetails dialogDetails;
    private SnackBarDetails snackBarDetails;
    private OfferDetails offerDetails;
    private CartConfigs cartConfigs;
    private MenuConfigs menuConfigs;
    private DeliverySlotConfigs deliverySlotConfigs;
    private Date timeStamp;
}
