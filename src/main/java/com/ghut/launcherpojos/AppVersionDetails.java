package com.ghut.launcherpojos;

import lombok.Data;

@Data
public class AppVersionDetails {
    private float appVersion;
    private boolean canBeSkipped;
    private int maxVersionDifferenceAllowedWhenSkipped;
    private String menuBarMessage;
}
