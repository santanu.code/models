package com.ghut.launcherpojos;

import lombok.Data;

@Data
public class TokenDetails {
    private String token;
    private String mobileNumber;
}
