package com.ghut.utils;

public class ConstantValues {

    public static final String OFFERS_NODE_NAME = "offers";
    public static final String APP_CONFIG_NODE_NAME = "app-configs";
    public static final String APP_EXTRA_CONFIG_NODE_NAME = "app-extra-configs";
    public static final String OFFERS_DETAILS_NODE_NAME = "offerDetails";
    public static final String SERVICEABLE_PINCODES_NODE_NAME = "serviceable_PinCodes";
    public static final String OUR_BRANCH_LOCATION_DETAILS_NODE_NAME = "ourBranchLocationDetails";
    public static final String SUBSCRIPTION_NODE_NAME = "subscription";
    public static final String SUBSCRIPTION_SUBNODE_MEALS = "meals";
    public static final String SUBSCRIPTION_DATADUMP_NODE_NAME = "subsciptionDataDump";
    public static final String ADMIN_CONFIG_NODE_NAME = "admin-configs";
    public static final String FEEDBACK_DETAILS_KEY = "feedbackDetails";
    public static final String PROMOCODES_APPLIED_BY_NODE = "promoCodesAppliedBy";
    public static String APP_NAME = "grannyzhut";
    //-------NODE NAMES----------
    public static final String ACTIVEMENU_NODE_NAME = "activeMenu";
    public static final String ACTIVEMENU_EN_NODE_NAME = "activeMenu_EN";
    public static final String USERS_NODE_NAME = "users";
    public static final String DEMO_USERS_NODE_NAME = "zDemoTestUsers";
    public static final String ORDERS_NODE_NAME = "orders";

    public static final String ACTIVEMENU_BENGALI_NODE_NAME = "activeMenu_bengali";

    public static final String PROMOCODE_NODE_NAME = "promocodes";
    public static final String PARTYORDERS_NODE_NAME = "partyOrders";
    public static final String APP_INVITES = "app-invites";
    public static final String HELP_SUPPORT_NODE = "help-and-support";
    public static final String NOTIFICATION_TOKEN_NODE_NAME = "notify_tokens";


    //----------------ORDER STATUS NODES-------------------------
    public static final String TO_BE_CONFIRMED_NODE_NAME = "toBeConfirmed";
    public static final String CONFIRMED_NODE_NAME = "confirmed";
    public static final String BEING_COOKED_NODE_NAME = "beingCooked";
    public static final String ON_THE_WAY_NODE_NAME = "onTheWay";
    public static final String DELIEVERED_NODE_NAME = "delivered";
    public static final String CANCELLED_NODE_NAME = "cancelled";


    //-------------FIREBASE------------------------
    public static String APP_BASE_LINK="https://eatbit-shan0508.firebaseio.com";
    public static String FIREBASE_SERVICEACCOUNT_DETAILS = ("{\n" +
            "  \"type\": \"service_account\",\n" +
            "  \"project_id\": \"eatbit-shan0508\",\n" +
            "  \"private_key_id\": \"0746ba9d194098aa8b3aded993095cd934c9045e\",\n" +
            "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDMwGjW+eZ/45NG\\n+DUBPXRN4gSv+AXzKOGXbRFm0KgNQircxLP60uguRXNiALsIH3zN44e4r/q3zvht\\n8L8LbsKwZFkNM3svzQVurVTlKoMEcmO0JgoyE0nXFaBfVxPlOtGz+4uVx7RkFWsv\\nrMilK/6bzIZtbNQN6C1FTJVbUMQvJWcyvkDXbpeMN+sjJwOYkE3qxYUk1S8C4u+5\\nV3jiC69U14+rAqZ5NLgQSrHy0JeM4tVOgjwIFSi1XMpSWvX/zYM9anSGCfefKgVt\\nCGrMQ+5YQA78gQqaizjkr1JPRmQtklzhJOlSjw/lnQ8/Wu5VJRIz0srZcJhemshK\\ni3VxFaeXAgMBAAECggEAI5e1Ad3aMFQet4ERMcB9lcZa+z35xs3N/XVDx6Qw+nEM\\no2j8QndkThrblmc7HAvhNac+YiARTN+JNw9ZHoMoC+K9yjovSV1x+s3dyqKLuPur\\nklbkgdTxipajVL994WTKrGdnWeen15gTlGuMS44syeeRXLdE25AHQpswAXo2SF2A\\nDBiCE1ryvk0Yia1tEHEPFQ8NNeeMZ7Zu2w36kJDkHK/2MNsTBOI0vqGg1tuKuIIg\\n1EVx1Ui4Yp6kYtgUGobSvkIqUFwnyFBUbS6lEjne04loCPru5BAlHZecLqgHh3A+\\nHh0nkvRrczC/+ggTAsEzgJpsXUZCUzS91iarIHv6EQKBgQDmNvKKCOPPtRqQUotf\\nki5dB6wQI1Q8Ve2CGwAumsV/HGEMkQubyqPjCyuRx6srpxeqyf+SNwf6MBomyVRH\\n5xYWnawEyvVUiro9c/p9nS8wDY+YVsk+yWU6KkXX89cuWZlVFGlR6Y8sLoNww3Zi\\nhlj6nBFaJp3Cp7vZTH5DY0AwxwKBgQDjr1kyyw7/UL395Db0Tibl6ExnYNSty98A\\nLJXs1+MFKh0UoJI0zfmcc/74bdTRBuKsoGQCupoSE3HIXYuvx42ccLbcs/VA0K8t\\nbccrzVKVBqdCj/HAZt8SodABsYyqXOpsUL5LgxxlYZf95oFB5L8e45XerZLnHm01\\nDyP8bHqisQKBgQC5pRvHBk43ZDLsqTPtOOOcb6BoTyURlrV10rI94t9alpcIXDsB\\nQdMRNyPGZj55cEBcoJIw5aHQNlQQbRBC/23HhRR/tO9LYqe3ALzB6SxxUq42Sd31\\nfNDdq9Mwx1kB7o0Al/oqbO7g0Q17tJPV9cLxLUOs8DEOFNcd71pumy3bZwKBgBU9\\nrJKigvmqrK7c9YuBJvvX2hji3bSBCoLXpfz8fv9iWGguOrWWaa0A3U6L4IjHnaoB\\nJJNkmS8KhVjt8TQq57kqgsTAPScY0N7qGOSrOQMSJdR93U3XywPP+LnPuJHELVKB\\njd85khKbMjTLpk9habsV/EtT4hqRqhqDGRZZgG9hAoGAXcaXmGQsNNBs5b5etQEI\\nNXJw5zmAw2Nu9BGM7j+s9drD7USqp5mezvQ4zcWK17wDQa/+YE+jXx2X0lwmq7At\\nka9SEelh4Co7BODHZP/UMZmggfXdZRg/kpIKa1ROwmE27I4v78htD870/qXoMVTW\\nkRuiAxwuGRK6Y1urttYx+1c=\\n-----END PRIVATE KEY-----\\n\",\n" +
            "  \"client_email\": \"firebase-adminsdk-gvmr4@eatbit-shan0508.iam.gserviceaccount.com\",\n" +
            "  \"client_id\": \"111676048589330889858\",\n" +
            "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
            "  \"token_uri\": \"https://accounts.google.com/o/oauth2/token\",\n" +
            "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
            "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-gvmr4%40eatbit-shan0508.iam.gserviceaccount.com\"\n" +
            "}\n");


    //----------------CACHE CONFIGS-----------------//
    public static final String MENU_CACHE = "menu-cache";
}
