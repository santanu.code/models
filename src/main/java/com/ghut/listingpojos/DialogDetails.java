package com.ghut.listingpojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DialogDetails {
    private String dialogTitle;
    private String dialogBody;
    private DialogType dialogType;
    @JsonProperty("isShowDialog")
    private boolean isShowDialog;

    public boolean isShowDialog() {
        return isShowDialog;
    }

    @JsonProperty("isShowDialog")
    public void setShowDialog(boolean showDialog) {
        isShowDialog = showDialog;
    }
}
