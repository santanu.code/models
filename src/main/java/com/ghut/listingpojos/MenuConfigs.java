package com.ghut.listingpojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MenuConfigs {
    private boolean isOpenNow;
    private String closedMessage;


    @JsonProperty("isOpenNow")
    public boolean isOpenNow() {
        return isOpenNow;
    }
    @JsonProperty("isOpenNow")
    public void setOpenNow(boolean openNow) {
        isOpenNow = openNow;
    }
}