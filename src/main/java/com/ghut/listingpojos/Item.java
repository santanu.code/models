package com.ghut.listingpojos;

import lombok.Data;

@Data
public class Item {
    private String itemIsActive;
    private String itemId;
    private String itemName;
    private String itemType;
    private String itemImageName;
    private String itemDescriptionSmall;
    private String itemDescriptionLarge;
}
