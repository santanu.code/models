package com.ghut.listingpojos;

import lombok.Data;

import java.util.List;

@Data
public class ItemListResponse {
    private List<MainItem> allItems;
    private String imageBaseURL;
}
