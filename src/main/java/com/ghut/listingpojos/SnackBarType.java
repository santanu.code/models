package com.ghut.listingpojos;

public enum SnackBarType {
    SHORT, LONG, INDEFINITE
}
