package com.ghut.listingpojos;

import lombok.Data;

@Data
public class QuantityPriceDetails {
    private double standardPrice;
    private double largePrice;
    private int qtyStandard;
    private int qtyLarge;
}
