package com.ghut.listingpojos;

import lombok.Data;

import java.util.List;

@Data
public class MainItem {
    private boolean mainItemIsActive;
    private String mainItemId;
    private String mainItemName;
    private String mainItemTag;
    private String mainItemType;
    private String mainItemCategory;
    private String mainItemImageName;
    private String mainItemDescriptionSmall;
    private String mainItemDescriptionLarge;
    private QuantityPriceDetails quantityPriceDetails;
    private float discountPercent;
    private int quantity;
    private List<Item> items;
}

