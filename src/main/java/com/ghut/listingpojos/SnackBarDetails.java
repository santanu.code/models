package com.ghut.listingpojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SnackBarDetails {
    private String activityName;
    @JsonProperty("isShowSnackBar")
    private boolean isShowSnackBar;
    private String snackBarMessage;
    private SnackBarType snackBarType;


    public boolean isShowSnackBar() {
        return isShowSnackBar;
    }
    @JsonProperty("isShowSnackBar")
    public void setShowSnackBar(boolean showSnackBar) {
        isShowSnackBar = showSnackBar;
    }
}
