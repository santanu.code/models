package com.ghut.adminpojos;

import com.ghut.orderspojos.OrderDetails;
import lombok.Data;

import java.util.Map;

@Data
public class AllOrderListResponse {
    private Map<String, OrderDetails> toBeConfirmed;
    private Map<String, OrderDetails> confirmed;
    private Map<String, OrderDetails> beingCooked;
    private Map<String, OrderDetails> onTheWay;
    private Map<String, OrderDetails> delivered;
    private Map<String, OrderDetails> cancelled;
}
