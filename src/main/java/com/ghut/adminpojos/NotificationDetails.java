package com.ghut.adminpojos;

import lombok.Data;

@Data
public class NotificationDetails {
    private String to;
    private PayloadData data;
}
