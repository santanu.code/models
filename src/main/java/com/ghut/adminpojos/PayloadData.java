package com.ghut.adminpojos;

import lombok.Data;

@Data
public class PayloadData {
    private String title;
    private String message;
    private String imageUrl;
}
