package com.ghut.adminpojos;

import lombok.Data;

@Data
public class AdminConfigs {
    private String appVersionNumber;
    private boolean updateRequired;
}
