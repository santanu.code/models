package com.ghut.adminpojos;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class NotificationSentResponse {
    @SerializedName("message_id")
    private String messageId;
}
