package com.ghut.adminpojos;


import com.ghut.orderspojos.OrderStateDetails;

import java.util.Map;

public class AllOrderStateDetails {
    private Map<String, OrderStateDetails> orderStateDetailsMap;

    public Map<String, OrderStateDetails> getOrderStateDetailsMap() {
        return orderStateDetailsMap;
    }

    public void setOrderStateDetailsMap(Map<String, OrderStateDetails> orderStateDetailsMap) {
        this.orderStateDetailsMap = orderStateDetailsMap;
    }
}
